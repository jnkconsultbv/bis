#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install apt-utils git wget curl libzip-dev zip unzip nodejs npm -yqq

# Install phpunit, the tool that we will use for testing
# curl --location --output /usr/local/bin/phpunit "https://phar.phpunit.de/phpunit.phar"
# chmod +x /usr/local/bin/phpunit

# Install mysql driver
# Here you can install any other extension that you need
docker-php-ext-install pdo_mysql zip bcmath
pecl install xdebug && docker-php-ext-enable xdebug
pecl install pcov && docker-php-ext-enable pcov 
