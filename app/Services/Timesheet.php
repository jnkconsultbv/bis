<?php

namespace App\Services;

use App\Enums\PriceTypeEnum;
use App\Models\Timesheet as ModelsTimesheet;
use Illuminate\Support\Carbon;

class Timesheet
{
    public function calculateTimeSheetPrice(ModelsTimesheet $timesheet): int
    {
        if ($timesheet->ended_at) {
            $from_date = Carbon::parse(date('Y-m-d H:i:s', strtotime($timesheet->started_at)));
            $through_date = Carbon::parse(date('Y-m-d H:i:s', strtotime($timesheet->ended_at)));
            switch ($timesheet->price_type) {
                case PriceTypeEnum::HOUR:
                    $shift_difference = $from_date->diffInMinutes($through_date);
                    return $timesheet->project->price_hour * $shift_difference / 60 / 100;
                    break;
                case PriceTypeEnum::DAY:
                    $shift_difference = $from_date->diffInDays($through_date);
                    return $timesheet->project->price_day / 100 * ($shift_difference + 1);
                    break;
                case PriceTypeEnum::FIX:
                    return $timesheet->project->price_fix / 100;
                    break;
            }
        } else {
            return 0;
        }
    }
}
