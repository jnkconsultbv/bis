<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Contact extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = ['firstname', 'lastname', 'email', 'phone', 'customer_id'];

    protected $searchableFields = ['*'];

    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }
}
