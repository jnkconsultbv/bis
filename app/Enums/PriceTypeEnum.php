<?php

namespace App\Enums;

enum PriceTypeEnum: int
{
case HOUR   = 1;
case DAY    = 2;
case FIX    = 3;
    }
