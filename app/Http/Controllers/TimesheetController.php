<?php

namespace App\Http\Controllers;

use App\Http\Requests\Timesheet\StoreRequest;
use App\Http\Requests\Timesheet\UpdateRequest;
use App\Models\Project;
use App\Models\Timesheet;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TimesheetController extends Controller
{
    public function index(Request $request): View
    {
        $search = $request->get('search', '');

        $timesheets = Timesheet::search($search)
            ->latest()
            ->with('project:id,name')
            ->with('project.customer')
            ->with('invoice')
            ->paginate(10);

        return view('timesheets.index', compact('timesheets', 'search'));
    }

    public function create(): View
    {
        $projects = Project::with('customer')->get();

        return view('timesheets.create', compact('projects'));
    }

    public function store(StoreRequest $request): RedirectResponse
    {
        Timesheet::create($request->validated());

        return redirect()
            ->route('timesheets.index')
            ->withSuccess('Timesheet has been created successfully.');
    }

    public function show(Timesheet $timesheet): View
    {
        return view('timesheets.show', compact('timesheet'));
    }

    public function edit(Timesheet $timesheet): View
    {
        $projects = Project::with('customer')->get();

        return view('timesheets.edit', compact('timesheet', 'projects'));
    }

    public function update(Timesheet $timesheet, UpdateRequest $request): RedirectResponse
    {
        $timesheet->update($request->validated());

        return redirect()
            ->route('timesheets.index')
            ->withSuccess('Timesheet Has Been updated successfully');
    }

    public function destroy(Timesheet $timesheet): RedirectResponse
    {
        $timesheet->delete();

        return redirect()
            ->route('timesheets.index')
            ->withSuccess('Timesheet has been deleted successfully');
    }
}
