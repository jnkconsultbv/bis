<?php

namespace App\Http\Controllers;

use App\Http\Requests\Project\StoreRequest;
use App\Http\Requests\Project\UpdateRequest;
use App\Models\Customer;
use App\Models\Project;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ProjectController extends Controller
{
    public function index(Request $request): View
    {
        $search = $request->get('search', '');

        $projects = Project::search($search)
            ->latest()
            ->withCount('timesheets')
            ->with('customer:id,name')
            ->paginate(10);

        return view('projects.index', compact('projects', 'search'));
    }

    public function create(): View
    {
        $customers = Customer::pluck('name', 'id');

        return view('projects.create', compact('customers'));
    }

    public function store(StoreRequest $request): RedirectResponse
    {
        Project::create($request->validated());

        return redirect()
            ->route('projects.index')
            ->withSuccess('Project has been created successfully.');
    }

    public function show(Project $project): View
    {
        return view('projects.show', compact('project'));
    }

    public function edit(Project $project): View
    {
        $customers = Customer::pluck('name', 'id');

        return view('projects.edit', compact('customers', 'project'));
    }

    public function update(Project $project, UpdateRequest $request): RedirectResponse
    {
        $project->update($request->validated());

        return redirect()
            ->route('projects.index')
            ->withSuccess('Project Has Been updated successfully');
    }

    public function destroy(Project $project): RedirectResponse
    {
        $project->delete();

        return redirect()
            ->route('projects.index')
            ->withSuccess('Project has been deleted successfully');
    }
}
