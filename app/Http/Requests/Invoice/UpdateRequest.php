<?php

namespace App\Http\Requests\Invoice;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'customer_id' => 'required|exists:customers,id',
            'invoice_year' => 'required',
            'invoice_month' => 'required',
            'invoice_increment' => 'required',
            'is_paid' => 'sometimes',
        ];
    }
}
