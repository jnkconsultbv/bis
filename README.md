## Laravel Bis APP: App for projects & invoices managments (in progress)

With Breeze & TailwindCss

[![pipeline status](https://gitlab.com/jnkconsult/bis/badges/master/pipeline.svg)](https://gitlab.com/jnkconsult/bis/-/commits/master)
[![coverage report](https://gitlab.com/jnkconsult/bis/badges/master/coverage.svg)](https://gitlab.com/jnkconsult/bis/-/commits/master)

-----

### How to use

- Clone the project with `git clone`
- Copy `.env.example` file to `.env` and edit database credentials there
- Run `composer install`
- Run `php artisan key:generate`
- Run `php artisan migrate --seed` (it has some seeded data for your countries, states and cities)
- Run `npm install`
- Run `npm run dev`
- That's it: launch the main URL


---

### PHP 
- version >=8.1