<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Customer;
use App\Models\Contact;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ContactsTest extends TestCase
{
    use DatabaseMigrations;
    
        /** @test */
        public function a_user_can_read_all_the_contacts()
        {
            $this->actingAs(User::factory()->create());
            $customer = Customer::factory()->create([]);
            $contact = Contact::factory()->create(['customer_id'=>$customer->id]);
            $response = $this->get('/customers/contacts');
            $response->assertSee($contact->email);
        }

        /** @test */
        public function a_user_can_read_single_contact()
        {
            $this->actingAs(User::factory()->create());
            $customer = Customer::factory()->create([]);
            $contact = Contact::factory()->create(['customer_id'=>$customer->id]);

            $response = $this->get('/customers/contacts/'.$contact->id);
            $response->assertSee($contact->firstname)
                ->assertSee($contact->lastname)
                ->assertSee($contact->email)
                ->assertSee($contact->phone)
                ->assertSee($contact->customer->name);
        }

        /** @test */
        public function authenticated_users_can_create_a_new_contact()
        {
            $this->actingAs(User::factory()->create());
            $customer = Customer::factory()->create([]);
            $contact = Contact::factory()->make(['customer_id'=>$customer->id]);
            $this->post('/customers/contacts',$contact->toArray());
            $this->assertEquals(1,Contact::all()->count());
        }

        /** @test */
        public function unauthenticated_users_cannot_create_a_new_contact()
        {
            $customer = Customer::factory()->create([]);
            $contact = Contact::factory()->make(['customer_id'=>$customer->id]);
            $this->post('/customers/contacts',$contact->toArray())
                ->assertRedirect('/login');
        }

        /** @test */
        public function a_contact_requires_a_firstname()
        {
            $this->actingAs(User::factory()->create());
            $customer = Customer::factory()->create([]);
            $contact = Contact::factory()->make(['customer_id'=>$customer->id, 'firstname'=>null]);
            $this->post('/customers/contacts',$contact->toArray())
                ->assertSessionHasErrors('firstname');
        }

        /** @test */
        public function a_contact_requires_a_lasttname()
        {
            $this->actingAs(User::factory()->create());
            $customer = Customer::factory()->create([]);
            $contact = Contact::factory()->make(['customer_id'=>$customer->id, 'lastname'=>null]);
            $this->post('/customers/contacts',$contact->toArray())
                ->assertSessionHasErrors('lastname');
        }

        /** @test */
        public function a_contact_requires_a_email()
        {
            $this->actingAs(User::factory()->create());
            $customer = Customer::factory()->create([]);
            $contact = Contact::factory()->make(['customer_id'=>$customer->id, 'email'=>null]);
            $this->post('/customers/contacts',$contact->toArray())
                ->assertSessionHasErrors('email');
        }

        /** @test */
        public function a_contact_dont_requires_a_phone()
        {
            $this->actingAs(User::factory()->create());
            $customer = Customer::factory()->create([]);
            $contact = Contact::factory()->make(['customer_id'=>$customer->id, 'phone'=>null]);
            $this->post('/customers/contacts',$contact->toArray())
                ->assertSessionHasNoErrors('phone');
        }

}