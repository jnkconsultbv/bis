<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Customer;
use App\Models\Contact;
use App\Models\Project;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ProjectsTest extends TestCase
{
    use DatabaseMigrations;
    
        /** @test */
        public function a_user_can_read_all_the_projects()
        {
            $this->actingAs(User::factory()->create());
            $customer = Customer::factory()->create([]);
            $project = Project::factory()->create(['customer_id'=>$customer->id]);
            $response = $this->get('/projects/projects');
            $response->assertSee($project->name);
        }

        /** @test */
        public function a_user_can_read_single_project()
        {
            $this->actingAs(User::factory()->create());
            $customer = Customer::factory()->create([]);
            $project = Project::factory()->create(['customer_id'=>$customer->id]);

            $response = $this->get('/projects/projects/'.$project->id);
            $response->assertSee($project->name)
                ->assertSee($project->price_hour)
                ->assertSee($project->price_day);
        }

        /** @test */
        public function authenticated_users_can_create_a_new_project()
        {
            $this->actingAs(User::factory()->create());
            $customer = Customer::factory()->create([]);
            $project = Project::factory()->create(['customer_id'=>$customer->id]);
            $this->post('/projects/projects',$project->toArray());
            $this->assertEquals(2,Project::all()->count());
        }

        /** @test */
        public function unauthenticated_users_cannot_create_a_new_project()
        {
            $customer = Customer::factory()->create([]);
            $project = Project::factory()->create(['customer_id'=>$customer->id]);
            $this->post('/projects/projects',$project->toArray())
                ->assertRedirect('/login');
        }

}