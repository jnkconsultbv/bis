<x-app-layout>
    <x-slot name="header">
        {{ __('Edit Timesheet') }}
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                <a href="{{ route('timesheets.index') }}"
                    class="inline-flex items-center px-4 py-2 mb-4 text-xs font-semibold tracking-widest text-white uppercase transition duration-150 ease-in-out bg-green-600 border border-transparent rounded-md hover:bg-green-500 active:bg-green-700 focus:outline-none focus:border-green-700 focus:shadow-outline-gray disabled:opacity-25">
                    <- Go back
                </a>
                <x-validation-errors class="mb-4" :errors="$errors" />
                <form action="{{ route('timesheets.update', $timesheet) }}" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="mb-4">
                        <x-label for="project_id" :value="__('timesheet.project')" class="required" />
                        <select class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                        name="project_id">
                            @foreach($projects as $value => $label)
                                <option value="{{ $value }}" {{ old('project_id', $timesheet->project->id) != $value ?: 'selected' }}>
                                    {{ $label->name }} ({{ $label->customer->name }})
                                </option>
                            @endforeach
                        </select>
                    </div>    
                    <div class="mb-4">
                        <x-label for="description" :value="__('timesheet.description')" class="required" />

                        <x-input id="description" class="block mt-1 w-full"
                        type="text"
                        name="description"
                        value="{{ old('description', $timesheet->description) }}"
                        required />
                    </div>
                    <div class="mb-4">
                        <x-label for="price_type" :value="__('timesheet.price_type')" class="required" />
                        <select class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                        name="price_type">
                            @foreach(App\Enums\PriceTypeEnum::cases() as $priceType)
                                <option value="{{ $priceType->value }}" {{ old('price_type', $timesheet->price_type) != $priceType->value ?: 'selected' }}>
                                    {{ $priceType->name }}
                                </option>
                            @endforeach
                        </select>
                        @error('price_type') <span class="text-red-500">{{ $message }}
                        </span>@enderror
                    </div>
                    <div class="mb-4">
                        <x-label for="started_at" :value="__('timesheet.started_at')" class="required" />

                        <x-input id="started_at" class="block mt-1 w-full"
                        type="dateTime-local"
                        name="started_at"
                        value="{{ old('started_at', date('Y-m-d\TH:m:s',  strtotime($timesheet->started_at))) }}"
                        required />
                    </div>
                    <div class="mb-4">
                        <x-label for="ended_at" :value="__('timesheet.ended_at')" />

                        <x-input id="ended_at" class="block mt-1 w-full"
                        type="dateTime-local"
                        name="ended_at"
                        value="{{ old('ended_at', date('Y-m-d\TH:m:s',  strtotime($timesheet->ended_at))) }}" />
                    </div>
                    <div>
                        <x-button>{{ __('customer.submit') }}</x-button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>