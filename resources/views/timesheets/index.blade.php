<x-app-layout>
    <x-slot name="header">
        {{ __('Timesheet list') }}
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                <div class="mb-5 mt-4">
                    <div class="flex flex-wrap justify-between">
                        <div class="md:w-1/2">
                            <x-table-search :search="$search"/>
                        </div>
                        <div class="md:w-1/2 text-right">
                            <x-table-create model="timesheets" />
                        </div>
                    </div>
                    <x-table-error />
                </div>
                <x-table>
                    <x-slot name="header">
                        <x-table-column>{{ __('Customer') }}</x-table-column>
                        <x-table-column>{{ __('Project') }}</x-table-column>
                        <x-table-column>{{ __('Description') }}</x-table-column>
                        <x-table-column>{{ __('Price type') }}</x-table-column>
                        <x-table-column>{{ __('Price') }}</x-table-column>
                        <x-table-column>{{ __('Started at') }}</x-table-column>
                        <x-table-column>{{ __('Ended at') }}</x-table-column>
                        <x-table-column>{{ __('Invoice') }}</x-table-column>
                        <x-table-column>{{ __('Action') }}</x-table-column>
                    </x-slot>
                    @forelse($timesheets as $row)
                        <tr>
                            <x-table-column><a href="{{ route('customers.show',$row->project->customer) }}" class="hover:underline">{{ $row->project->customer->name }}</a></x-table-column>
                            <x-table-column><a href="{{ route('projects.show',$row->project) }}" class="hover:underline">{{ $row->project->name }}</a></x-table-column>
                            <x-table-column>{{ $row->description ?? '-' }}</x-table-column>
                            <x-table-column>{{ $row->price_type->name ?? '-' }}</x-table-column><x-table-column><x-format-amount :amount="$row->price" currency="eur" locale="fr_BE" /></x-table-column>
                            <x-table-column>{{ $row->started_at ?? '-' }}</x-table-column>
                            <x-table-column>{{ $row->ended_at ?? '-' }}</x-table-column>
                            <x-table-column>{{ optional($row->invoice)->invoice_year ?? '-' }}{{ optional($row->invoice)->invoice_month ?? '-' }}{{ optional($row->invoice)->invoice_increment ?? '-' }}</x-table-column>
                            <x-table-column>
                                <x-table-action model="timesheets" :row="$row" :condition="$row->invoice"/>
                            </x-table-column>
                        </tr>
                    @empty
                        <tr>
                            <td class="px-4 py-2 border text-red-500" colspan="8">{{ __('No timesheets found.') }}</td>
                        </tr>
                    @endforelse
                    <x-slot name="pagination">
                        {{ $timesheets->links() }}    
                    </x-slot>
                </x-table>
            </div>
        </div>
    </div>
</x-app-layout>