<x-app-layout>
    <x-slot name="header">
        {{ __('Invoices list') }}
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                <div class="mb-5 mt-4">
                    <div class="flex flex-wrap justify-between">
                        <div class="md:w-1/2">
                            <x-table-search :search="$search"/>
                        </div>
                        <div class="md:w-1/2 text-right">
                            <x-table-create model="invoices" />
                        </div>
                    </div>
                    <x-table-error />
                </div>
                <x-table>
                    <x-slot name="header">
                        <x-table-column>{{ __('Customer') }}</x-table-column>
                        <x-table-column>{{ __('Date creation') }}</x-table-column>
                        <x-table-column>{{ __('Reference') }}</x-table-column>
                        <x-table-column>{{ __('Total Without VAT') }}</x-table-column>
                        <x-table-column>{{ __('Is paid ?') }}</x-table-column>
                        <x-table-column>{{ __('Action') }}</x-table-column>
                    </x-slot>
                    @forelse($invoices as $row)
                        <tr>
                            <x-table-column><a href="{{ route('customers.show',$row->customer) }}" class="hover:underline">{{ $row->customer->name }}</a></x-table-column>
                            <x-table-column>{{ $row->created_at }}</x-table-column>
                            <x-table-column>{{ $row->invoice_year }}{{ $row->invoice_month }}{{ $row->invoice_increment }}</x-table-column>
                            <x-table-column><x-format-amount :amount="$row->total_without_vat" currency="eur" locale="fr_BE" /> <span class="bg-green-100 text-green-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded-full dark:bg-green-200 dark:text-green-900">{{ $row->timesheets_count }}</span></x-table-column>
                            <x-table-column>{{ $row->is_paid }}</x-table-column>
                            <x-table-column>
                                <x-table-action model="invoices" :row="$row" :condition="$row->is_paid"/>
                            </x-table-column>
                        </tr>
                    @empty 
                        <tr>
                            <td class="px-4 py-2 border text-red-500" colspan="6">{{ __('No invoices found.') }}</td>
                        </tr>
                    @endforelse
                    <x-slot name="pagination">
                        {{ $invoices->links() }}    
                    </x-slot>
                </x-table>
            </div>
        </div>
    </div>
</x-app-layout>