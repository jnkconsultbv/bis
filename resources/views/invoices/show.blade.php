<x-app-layout>
    <x-slot name="header">
        {{ __('Show Invoice') }}
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                <a href="{{ route('invoices.index') }}"
                    class="inline-flex items-center px-4 py-2 mb-4 text-xs font-semibold tracking-widest text-white uppercase transition duration-150 ease-in-out bg-green-600 border border-transparent rounded-md hover:bg-green-500 active:bg-green-700 focus:outline-none focus:border-green-700 focus:shadow-outline-gray disabled:opacity-25">
                    <- Go back
                </a>
                <table class="w-full table-fixed">
                    <tbody>
                        <tr>
                            <td class="px-4 py-2 font-bold">Company</td>
                            <td>{{ $invoice->customer->name }}</td>
                        </tr>
                        <tr>
                            <td class="px-4 py-2 font-bold">Year</td>
                            <td>{{ $invoice->invoice_year }}</td>
                        </tr>
                        <tr>
                            <td class="px-4 py-2 font-bold">Month</td>
                            <td>{{ $invoice->invoice_month }}</td>
                        </tr>
                        <tr>
                            <td class="px-4 py-2 font-bold">Increment</td>
                            <td>{{ $invoice->invoice_increment }}</td>
                        </tr>
                        <tr>
                            <td class="px-4 py-2 font-bold">price without VAT</td>
                            <td>{{ $invoice->total_without_vat }}</td>
                        </tr>
                        <tr>
                            <td class="px-4 py-2 font-bold">VAT</td>
                            <td>{{ $invoice->total_vat }}</td>
                        </tr>
                        <tr>
                            <td class="px-4 py-2 font-bold">Price with VAT</td>
                            <td>{{ $invoice->total_with_vat }}</td>
                        </tr>
                        <tr>
                            <td class="px-4 py-2 font-bold">Is Paid</td>
                            <td>{{ $invoice->is_paid }}</td>
                        </tr>
                        <tr>
                            <td class="px-4 py-2 font-bold">Created on</td>
                            <td>{{ date_format($invoice->created_at, 'jS F Y g:i A') }}</td>
                        </tr>
                        <tr>
                            <td class="px-4 py-2 font-bold">Last updated</td>
                            <td>{{ date_format($invoice->updated_at, 'jS F Y g:i A') }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-app-layout>