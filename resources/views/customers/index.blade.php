<x-app-layout>
    <x-slot name="header">
        {{ __('Customer list') }}
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                <div class="mb-5 mt-4">
                    <div class="flex flex-wrap justify-between">
                        <div class="md:w-1/2">
                            <x-table-search :search="$search"/>
                        </div>
                        <div class="md:w-1/2 text-right">
                            <x-table-create model="customers" />
                        </div>
                    </div>
                    <x-table-error />
                </div>
                <x-table>
                    <x-slot name="header">
                        <x-table-column>{{ __('Name') }}</x-table-column>
                        <x-table-column>{{ __('VAT') }}</x-table-column>
                        <x-table-column>{{ __('Contacts') }}</x-table-column>
                        <x-table-column>{{ __('Locations') }}</x-table-column>
                        <x-table-column>{{ __('Projects') }}</x-table-column>
                        <x-table-column>{{ __('Action') }}</x-table-column>
                    </x-slot>
                    @forelse($customers as $row)
                        <tr>
                            <x-table-column>{{ $row->name ?? '-' }}</x-table-column>
                            <x-table-column>{{ $row->vat ?? '-' }}</x-table-column>
                            <x-table-column><a href="{{ route('contacts.index',['search'=>$row->id]) }}"><span class="bg-green-100 text-green-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded-full dark:bg-green-200 dark:text-green-900">{{ $row->contacts_count }}</span></a></x-table-column>
                            <x-table-column><a href="{{ route('locations.index',['search'=>$row->id]) }}"><span class="bg-green-100 text-green-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded-full dark:bg-green-200 dark:text-green-900">{{ $row->locations_count }}</span></a></x-table-column>
                            <x-table-column><a href="{{ route('projects.index',['search'=>$row->id]) }}"><span class="bg-green-100 text-green-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded-full dark:bg-green-200 dark:text-green-900">{{ $row->projects_count }}</span></a></x-table-column>
                            <x-table-column>
                                <x-table-action model="customers" :row="$row"/>
                            </x-table-column>
                        </tr>
                    @empty 
                        <tr>
                            <td class="px-4 py-2 border text-red-500" colspan="6">{{ __('No contact found.') }}<</td>
                        </tr>
                    @endforelse
                    <x-slot name="pagination">
                        {{ $customers->links() }}
                    </x-slot>
                </x-table>
            </div>
        </div>
    </div>
</x-app-layout>