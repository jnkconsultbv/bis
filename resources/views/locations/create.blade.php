<x-app-layout>
    <x-slot name="header">
        {{ __('Create Location') }}
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                <a href="{{ route('locations.index') }}"
                    class="inline-flex items-center px-4 py-2 mb-4 text-xs font-semibold tracking-widest text-white uppercase transition duration-150 ease-in-out bg-green-600 border border-transparent rounded-md hover:bg-green-500 active:bg-green-700 focus:outline-none focus:border-green-700 focus:shadow-outline-gray disabled:opacity-25">
                    <- Go back
                </a>
                <x-validation-errors class="mb-4" :errors="$errors" />
                <form action="{{ route('locations.store') }}" method="POST" >
                    @csrf
                    <div class="mb-4">
                        <x-label for="customer_id" :value="__('location.customer')" class="required" />

                        <select name="customer_id" id="customer_id" 
                        class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" required>
                            <option disabled hidden {{ old('customer_id') != null ?: 'selected' }}>
                                {{ __('location.select') }}
                            </option>
                            @foreach($customers as $value => $label)
                                <option value="{{ $value }}" {{ old('customer_id') != $value ?: 'selected' }}>
                                    {{ $label }}
                                </option>
                            @endforeach
                        </select>
                    </div>    
                    <div class="mb-4">
                        <x-label for="street" :value="__('location.street')" class="required" />

                        <x-input id="street" class="block mt-1 w-full"
                        type="text"
                        name="street"
                        value="{{ old('street') }}"
                        required />
                    </div>
                    <div class="mb-4">
                        <x-label for="street_number" :value="__('location.street_number')" class="required" />

                        <x-input id="street_number" class="block mt-1 w-full"
                        type="text"
                        name="street_number"
                        value="{{ old('street_number') }}"
                        required />
                    </div>
                    <div class="mb-4">
                        <x-label for="street_box" :value="__('location.street_box')" />

                        <x-input id="street_box" class="block mt-1 w-full"
                        type="text"
                        name="street_box"
                        value="{{ old('street_box') }}" />
                    </div>
                    <div class="mb-4">
                        <x-label for="phone" :value="__('customer.phone')" />

                        <x-input id="phone" class="block mt-1 w-full"
                        type="text"
                        name="phone"
                        value="{{ old('phone') }}" />
                    </div>
                    <div class="mb-4">
                        <x-label for="country_id" :value="__('customer.country')" class="required" />

                        <select name="country_id" id="country_id" class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline">
                            <option value="">Select Country</option>
                            @foreach ($countries as $value => $label)
                            <option value="{{ $value }}" {{ old('country_id') != $value ?: 'selected' }}>
                                {{ $label }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-4">
                        <x-label for="state_id" :value="__('customer.state')" class="required" />
                        <select name="state_id" id="state_id" class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline">
                        </select>
                    </div>
                    <div class="mb-4">
                        <x-label for="city_id" :value="__('customer.city')" class="required" />
                        <select name="city_id" id="city_id" class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline">
                        </select>
                    </div>
                    <div class="mb-4">
                        <x-label for="zip" :value="__('location.zip')" class="required" />

                        <x-input id="zip" class="block mt-1 w-full"
                        type="text"
                        name="zip"
                        value="{{ old('zip') }}"
                        required />
                    </div>
                    <div>
                        <x-button>{{ __('customer.submit') }}</x-button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#country_id').on('change', function () {
                var idCountry = this.value;
                $("#state_id").html('');
                $.ajax({
                    url: "{{url('api/fetch-states')}}",
                    type: "POST",
                    data: {
                        country_id: idCountry,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (result) {
                        $('#state_id').html('<option value="">Select State</option>');
                        $.each(result.states, function (key, value) {
                            $("#state_id").append('<option value="' + value
                                .id + '">' + value.name + '</option>');
                        });
                        $('#city_id').html('<option value="">Select City</option>');
                    }
                });
            });
            $('#state_id').on('change', function () {
                var idState = this.value;
                $("#city_d").html('');
                $.ajax({
                    url: "{{url('api/fetch-cities')}}",
                    type: "POST",
                    data: {
                        state_id: idState,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (res) {
                        $('#city_id').html('<option value="">Select City</option>');
                        $.each(res.cities, function (key, value) {
                            $("#city_id").append('<option value="' + value
                                .id + '">' + value.name + '</option>');
                        });
                    }
                });
            });
        });
    </script>
</x-app-layout>