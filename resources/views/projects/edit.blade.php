<x-app-layout>
    <x-slot name="header">
        {{ __('Edit Project') }}
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                <a href="{{ route('projects.index') }}"
                    class="inline-flex items-center px-4 py-2 mb-4 text-xs font-semibold tracking-widest text-white uppercase transition duration-150 ease-in-out bg-green-600 border border-transparent rounded-md hover:bg-green-500 active:bg-green-700 focus:outline-none focus:border-green-700 focus:shadow-outline-gray disabled:opacity-25">
                    <- Go back
                </a>
                <x-validation-errors class="mb-4" :errors="$errors" />
                <form action="{{ route('projects.update', $project) }}" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="mb-4">
                        <x-label for="customer_id" :value="__('project.customer')" class="required" />

                        <select name="customer_id" id="customer_id" 
                        class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" required>
                            <option disabled hidden {{ old('customer_id', $project->customer->id) }}!= null ?: 'selected' }}>
                                {{ __('project.select') }}
                            </option>
                            @foreach($customers as $value => $label)
                                <option value="{{ $value }}" {{ old('customer_id', $project->customer->id) != $value ?: 'selected' }}>
                                    {{ $label }}
                                </option>
                            @endforeach
                        </select>
                    </div>    
                    <div class="mb-4">
                        <x-label for="name" :value="__('project.name')" class="required" />

                        <x-input id="name" class="block mt-1 w-full"
                        type="text"
                        name="name"
                        value="{{ old('name', $project->name) }}"
                        required />
                    </div>
                    <div class="mb-4">
                        <x-label for="description" :value="__('project.description')" />

                        <textarea id="description" name="description" class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
                            {{ old('description', $project->description) }}
                        </textarea>
                    </div>
                    <div class="mb-4">
                        <x-label for="price_hour" :value="__('project.price_hour')" class="required" />

                        <x-input id="price_hour" class="block mt-1 w-full"
                        type="number"
                        name="price_hour"
                        step="0.01" min="0" max="100"
                        value="{{ old('price_hour', $project->price_hour) }}"
                        required />
                    </div>
                    <div class="mb-4">
                        <x-label for="price_day" :value="__('project.price_day')" class="required" />

                        <x-input id="price_day" class="block mt-1 w-full"
                        type="number"
                        name="price_day"
                        step="0.01" min="0" max="1000"
                        value="{{ old('price_day', $project->price_day) }}"
                        required />
                    </div>
                    <div class="mb-4">
                        <x-label for="price_fix" :value="__('project.price_fix')" />

                        <x-input id="price_fix" class="block mt-1 w-full"
                        type="number"
                        name="price_fix"
                        step="0.01" min="0" max="100000"
                        value="{{ old('price_fix', $project->price_fix) }}" />
                    </div>
                    <div class="mb-4">
                        <x-label for="started_at" :value="__('project.started_at')" />

                        <x-input id="started_at" class="block mt-1 w-full"
                        type="date"
                        name="started_at"
                        value="{{ old('started_at', $project->started_at) }}" />
                    </div>
                    <div class="mb-4">
                        <x-label for="ended_at" :value="__('project.ended_at')" />

                        <x-input id="ended_at" class="block mt-1 w-full"
                        type="date"
                        name="ended_at"
                        value="{{ old('ended_at', $project->ended_at) }}" />
                    </div>
                    <div>
                        <x-button>{{ __('customer.submit') }}</x-button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>