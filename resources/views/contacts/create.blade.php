<x-app-layout>
    <x-slot name="header">
        {{ __('Create Contacts') }}
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                <a href="{{ route('contacts.index') }}"
                    class="inline-flex items-center px-4 py-2 mb-4 text-xs font-semibold tracking-widest text-white uppercase transition duration-150 ease-in-out bg-green-600 border border-transparent rounded-md hover:bg-green-500 active:bg-green-700 focus:outline-none focus:border-green-700 focus:shadow-outline-gray disabled:opacity-25">
                    <- Go back
                </a>
                <x-validation-errors class="mb-4" :errors="$errors" />
                <form action="{{ route('contacts.store') }}" method="POST" >
                    @csrf
                    <div class="mb-4">
                        <x-label for="customer_id" :value="__('contact.customer')" class="required" />

                        <select name="customer_id" id="customer_id" 
                        class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" required>
                            <option disabled hidden {{ old('customer_id') != null ?: 'selected' }}>
                                {{ __('contact.select') }}
                            </option>
                            @foreach($customers as $value => $label)
                                <option value="{{ $value }}" {{ old('customer_id') != $value?: 'selected' }}>
                                    {{ $label }}
                                </option>
                            @endforeach
                        </select>
                    </div>    
                    <div class="mb-4">
                        <x-label for="firstname" :value="__('customer.firstname')" class="required" />

                        <x-input id="firstname" class="block mt-1 w-full"
                        type="text"
                        name="firstname"
                        value="{{ old('firstname') }}"
                        required />
                    </div>
                    <div class="mb-4">
                        <x-label for="lastname" :value="__('customer.lastname')" class="required" />

                        <x-input id="lastname" class="block mt-1 w-full"
                        type="text"
                        name="lastname"
                        value="{{ old('lastname') }}"
                        required />
                    </div>
                    <div class="mb-4">
                        <x-label for="email" :value="__('customer.email')" class="required" />

                        <x-input id="email" class="block mt-1 w-full"
                        type="email"
                        name="email"
                        value="{{ old('email') }}"
                        required />
                    </div>
                    <div class="mb-4">
                        <x-label for="phone" :value="__('customer.phone')" />

                        <x-input id="phone" class="block mt-1 w-full"
                        type="text"
                        name="phone"
                        value="{{ old('phone') }}"
                        required />
                    </div>
                    <div>
                        <x-button>{{ __('customer.submit') }}</x-button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>